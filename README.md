### README ###

Need to use WASD to shoot, SPACE to jump.

All 4 coins need to be collected, and all 3 enemies need to be stomped and the end reached in order to finish the level.

Restart button works.

There was stuff for a projectile, but I couldn't get it to work properly, and I didn't do enough to make it worth it.

Have to attribute for musical artist:

Got my music specifically from sawsquaremusic on FreeMusicArchive

https://freemusicarchive.org/music/sawsquarenoise/dojokratos

https://freemusicarchive.org/music/sawsquarenoise/Towel_Defence_OST

Eveything else is CC0 unless stated otherwise.