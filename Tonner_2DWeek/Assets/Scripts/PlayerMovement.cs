﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public GameObject deathCamera;
    public float shotSpeed = 100f;
    public int lives;
    public int score;
    public int shotCount;
    public float runSpeed;
    public float jumpForce;
    public SpriteRenderer spriteRenderer;
    public Animator animator;

    public bool door = true;
    public GameObject collectable1;
    public GameObject collectable2;
    public GameObject collectable3;
    public GameObject collectable4;


    public GameObject enemy;
    public GameObject enemy2;
    public GameObject enemy3;

    public GameObject powerUp;

    public GameObject projectilePrefab;

    public GameObject levelExit;

    public GameObject winText;
    public GameObject loseText;
    public TextMeshProUGUI lifeText;
    public TextMeshProUGUI scoreText;
    public Button buttonTextObject;
    public GameObject buttonTextObject2;
    public TextMeshProUGUI shotText;
    public GameObject ammoPicture;

    public AudioSource m_AudioSource; //Jump
    public AudioSource m_AudioSource2; // Lose Music
    public AudioSource m_AudioSource3; // Coin
    public AudioSource m_AudioSource4; // Background Music
    public AudioSource m_AudioSource5; // Win Music
    public AudioSource m_AudioSource6; // Shoot
    public AudioSource m_AudioSource7; // PlayerHurt
    public AudioSource m_AudioSource8; // EnemyHurt

    public Transform shotSpawn;

    public GameObject selfPlayer;

    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = new Vector3(0, 0, 0);
        m_AudioSource2.Stop();
        m_AudioSource5.Stop();
        m_AudioSource4.Play();
        selfPlayer.SetActive(true);
        door = true;
        deathCamera.SetActive(false);
        enemy.SetActive(true);
        enemy2.SetActive(true);
        enemy3.SetActive(true);
        winText.SetActive(false);
        buttonTextObject.onClick.AddListener(TaskOnClick);
        loseText.SetActive(false);
        buttonTextObject2.SetActive(false);
        collectable1.SetActive(true);
        collectable2.SetActive(true);
        collectable3.SetActive(true);
        collectable4.SetActive(true);
        rB2D = GetComponent<Rigidbody2D>();
        lives = 3;
        score = 0;
        shotCount = 10;

        SetLivesText();

        SetScoreText();

        SetShotCount();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
        if (shotCount > 0) 
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
                m_AudioSource6.Play();
                Rigidbody2D projectileRB = projectile.GetComponent<Rigidbody2D>();
                projectileRB.velocity = new Vector2(projectileRB.velocity.x * shotSpeed, 0);
                shotCount--;
                SetShotCount();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Start();
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        float verticalInput = Input.GetAxis("Vertical");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
        {
            spriteRenderer.flipX = false;
        }
        if (rB2D.velocity.x < 0)
        {
            spriteRenderer.flipX = true;
        }

        if (Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        if (verticalInput < 0f)
        {
            animator.SetBool("isInAir", true);
        }
        else
        {
            animator.SetBool("isInAir", false);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Death"))
        {
            transform.localPosition = new Vector3(0, 0, 0);
            lives -= 10;
            m_AudioSource2.Play();
            rB2D.velocity = Vector2.zero;
            SetLivesText();
        }
        if (other.CompareTag("Enemy"))
        {
            m_AudioSource7.Play();
            lives--;
            SetLivesText();
            Change2();
        }
        if (other.CompareTag("EnemyDeath"))
        {
            m_AudioSource8.Play();
            other.gameObject.SetActive(false);
            score += 50;
            SetScoreText();
        }

        if (other.CompareTag("Coin"))
        {
            score += 50;
            m_AudioSource3.Play();
            SetScoreText();
            other.gameObject.SetActive(false);
        }
        if (other.CompareTag("GameWin"))
        {
            //print("Door");
            door = false;
            SetScoreText();
        }
    }


    void Jump()
    {
        m_AudioSource.Play();
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    void TaskOnClick()
    {
        selfPlayer.SetActive(true);
        rB2D.velocity = Vector2.zero;

        transform.localPosition = new Vector3(0, 0, 0);

        Start();
    }

    void Change2()
    {
        //rB2D.angularVelocity = Vector2.zero;
        rB2D.velocity = Vector2.zero;

        transform.localPosition = new Vector3(0, 0, 0);

    }

        public void SetLivesText()
    {
        lifeText.text = "Lives: " + lives.ToString();
        if (lives <= 0)
        {
            loseText.SetActive(true);
            buttonTextObject2.SetActive(true);
            m_AudioSource4.Stop();
            m_AudioSource2.Play();
            deathCamera.SetActive(true);
            selfPlayer.SetActive(false);
        }
    }

    public void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();

        if (score >= 350 && door == false)
         {
             winText.SetActive(true);
             buttonTextObject2.SetActive(true);
             m_AudioSource4.Stop();
             m_AudioSource5.Play();
             deathCamera.SetActive(true);
             selfPlayer.SetActive(false);
         } 
    }

    public void SetShotCount()
    {
        shotText.text = shotCount.ToString();
    }
}
