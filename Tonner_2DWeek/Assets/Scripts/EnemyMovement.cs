﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public GameObject enemySelf;
    public GameObject Waypoint1;
    public GameObject Waypoint2;
    public SpriteRenderer spriteRenderer;
    public Animator animator;

    Vector3 enemyx;
    Vector3 enemyx2;

    public float speed = 0.05f;


    void Start()
    {
        enemyx = Waypoint1.transform.position;
        enemyx2 = Waypoint2.transform.position;

    }

    void Update()
    {
        float progress = Mathf.PingPong(Time.time * speed, 1f);
        transform.position = Vector2.Lerp(enemyx, enemyx2, progress);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Waypoint1"))
        {
            //print("Worked");
            animator.SetBool("HasHitLeft", false);
        }
        if (other.gameObject.CompareTag("Waypoint2"))
        {
            //print("Worked");
            animator.SetBool("HasHitLeft", true);
        }
    }

}

