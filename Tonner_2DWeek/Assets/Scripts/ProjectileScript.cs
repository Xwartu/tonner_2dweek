﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{

  public GameObject player;
 void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger || !player)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                Destroy(other.gameObject);
            }
            Destroy(gameObject);

        }
    }
}
